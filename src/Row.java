/**
 * Row
 */

public class Row {
    private String s1;
    private String s2;

    public Row(String s1, String s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    public String getS1() { return s1; }

    public String getS2() { return s2; }

    public void setS2(String s) { s2 = s; }

    @Override
    public int hashCode(){
        int code;
        int hash = 13;
        code = s1 == null ? 0 : s1.hashCode();
        hash = 31 * hash + code;
        code = s2 == null ? 0 : s2.hashCode();
        hash = 31 * hash + code;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null || !(obj instanceof Row))
            return false;
        Row c = (Row) obj;
        return s1.equals(c.getS1()) && s2.equals(c.getS2());
    }

    @Override
    public String toString() {
        return s1.concat(" = ").concat(s2);
    }
}