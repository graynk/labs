import java.io.Serializable;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Characteristic implements Serializable {
    private String name;
    private ObservableList<String> values = FXCollections.observableArrayList();
    private boolean current;
    private String selected;
    
    public Characteristic(String name, ObservableList<String> add){
        this.name = name;
        //this.values.addAll(Arrays.asList(add));
        this.values = add;
        this.current = false;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the values
     */
    public String getValue(int i) {
        return values.get(i);
    }
    
    public ObservableList<String> getValues() {
        return values;
    }

    /**
     * @param value the values to set
     */
    public void addValue(String value) {
        values.add(value);
    }

    /**
     * @return the current
     */
    public boolean isCurrent() {
        return current;
    }

    /**
     * @param current the current to set
     */
    public void setCurrent(boolean current) {
        this.current = current;
    }

    public void setSelected(String selected) { this.selected = selected; }

    public String getSelected() { return selected; }

    @Override
    public String toString() {
        return name;
    }

    public String getThatOtherString() {
        return name.concat(" = ").concat(selected);
    }
    
}
