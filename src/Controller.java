import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;

/**
 * Контроллер
 */

public class Controller {
    @FXML private TextField factNameField = new TextField();
    @FXML private TextField factValueField = new TextField();
    @FXML private TableView<Characteristic> factsTable;
    @FXML private TableView<Row> analysisTable;
    @FXML private TableView<Row> rulesTable;
    @FXML private TableView<Characteristic> predicateTable;
    @FXML private TableView<Characteristic> consequenceTable;
    @FXML private ComboBox<Characteristic> predicateComboBox;
    @FXML private ComboBox<Characteristic> consequenceComboBox;
    @FXML private ComboBox<Characteristic> analysisComboBox;
    @FXML private ComboBox<String> analysisValueComboBox;
    private ObservableMap<String, ObservableList<String>> mapData = FXCollections.observableHashMap();
    private ObservableMap<Row, ObservableList<Row>> rulesData = FXCollections.observableHashMap();
    private ObservableList<Characteristic> data = FXCollections.observableArrayList();

    @SuppressWarnings("unchecked")

    void setup() {
        factsTable.setItems(data);
        factsTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        factsTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("values"));

        predicateTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        ((TableColumn<Characteristic, String>)predicateTable.getColumns().get(1)).setCellFactory(p -> new ComboBoxCell());
        consequenceTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        ((TableColumn<Characteristic, String>)consequenceTable.getColumns().get(1)).setCellFactory(p -> new ComboBoxCell());

        analysisTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("s1"));
        analysisTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("s2"));

        rulesTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("s1"));
        rulesTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("s2"));

        data.addListener((ListChangeListener<Characteristic>) c -> {
            while(c.next()){
                if(c.wasAdded()){
                    List<? extends Characteristic> list = c.getAddedSubList();
                    for(Characteristic s : list){
                        analysisComboBox.getItems().add(s);
                        predicateComboBox.getItems().add(s);
                        consequenceComboBox.getItems().add(s);
                    }
                }
            }
        });

        analysisComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            analysisValueComboBox.setItems(mapData.get(newValue.getName()));
        });
    }

    public void addToFacts(){
        String name = factNameField.getText();
        String value = factValueField.getText();
        if(mapData.containsKey(name)){
            ObservableList<String> list = mapData.get(name);
            if(list.contains("")) list.remove("");
            if(!list.contains(value) && !value.equals("")) mapData.get(name).add(value);
            factsTable.refresh();
        }
        else {
            ObservableList<String> list = FXCollections.observableArrayList();
            list.add(value);
            data.add(new Characteristic(name, list));
            mapData.put(name, list);
        }
    }

    public void addPredicate() {
        //predicateTable.getItems().add(data.get(data.indexOf(predicateComboBox.getSelectionModel().getSelectedItem())));
        predicateTable.getItems().add(data.get(predicateComboBox.getSelectionModel().getSelectedIndex()));
    }

    public void addConsequence() {
        consequenceTable.getItems().add(data.get(consequenceComboBox.getSelectionModel().getSelectedIndex()));
    }

    public void addRule() {
        ObservableList<Characteristic> predicates = predicateTable.getItems();
        ObservableList<Characteristic> consequences = consequenceTable.getItems();

        for(Characteristic consequence : consequences) {
            Row row = new Row(consequence.getName(), consequence.getSelected());
            if(!rulesData.containsKey(row)) {
                ObservableList<Row> list = FXCollections.observableArrayList();
                for(Characteristic predicate : predicates) {
                    list.add(new Row(predicate.getName(), predicate.getSelected()));
                }
                rulesData.put(row, list);
                rulesTable.getItems().add(new Row(listToString(list), consequence.getThatOtherString()));
            }
            else {
                ObservableList<Row> list = rulesData.get(row);
                for(Characteristic predicate : predicates) {
                    Row otherRow = new Row(predicate.getName(), predicate.getSelected());
                    if(!list.contains(otherRow)) list.add(otherRow);
                    rulesTable.refresh();
                }
            }
        }
        clearRules();
    }

    public void addToAnalysis() {
        String s1 = data.get(data.indexOf(analysisComboBox.getSelectionModel().getSelectedItem())).getName();
        String s2 = analysisValueComboBox.getSelectionModel().getSelectedItem();
        Row object = new Row(s1, s2);
        ObservableList<Row> items = analysisTable.getItems();
        if(!items.contains(object)) items.add(object);
        /**
         * абсолютно ужасный и не эффективный алгоритм, но кого волнует, мне лень.
         * очень ужасный
         * серьезно
         * сам не знаю как оно работает
         * на самом деле оно не работает, а притворяется
         * четыре цикла появились тут сами, я ничего не делал, иншалла
         */
        boolean mark = true;
        boolean breakout = false;
        while(mark) {
            for(Row key : rulesData.keySet()){
                if(rulesData.containsKey(key)){
                    for(Row value : rulesData.get(key)) {
                        for(Row item : items) {
                            /*if(item.getS1().equals(key.getS1())) {
                                item.setS2(key.getS2());
                                break;
                            }*/
                            if(item.equals(value) && !items.contains(key)) { mark = true; break; }
                            else {
                                mark = false;
                                breakout = false;
                            }
                        }
                        if(mark && !items.contains(key)) {
                            items.add(key);
                            breakout = true;
                            break;
                        }
                    }
                }
                if(breakout) break;
            }
        }
        rulesTable.refresh();
    }

    public void clearFacts() {
        data.clear();
        mapData.clear();
        consequenceComboBox.getItems().clear();
        predicateComboBox.getItems().clear();
        analysisComboBox.getItems().clear();
        analysisValueComboBox.getItems().clear();
        deleteRules();
        clearAnalysis();
    }

    public void clearRules() {
        predicateTable.getItems().clear();
        consequenceTable.getItems().clear();
    }

    public void deleteRules() {
        clearRules();
        rulesData.clear();
        rulesTable.getItems().clear();
    }

    public void clearAnalysis() {
        analysisTable.getItems().clear();
    }

    private String listToString(List<Row> list) {
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < list.size(); i++) {
            builder.append(list.get(i));
            if(list.size() > 1 && i != list.size() - 1) builder.append(" И ");
        }
        return builder.toString();
    }
}

class ComboBoxCell extends TableCell<Characteristic, String>
{

    private ComboBox<String> comboBox;


    public ComboBoxCell()
    {
        comboBox = new ComboBox<>();
        comboBox.setMaxSize(Double.MAX_VALUE, USE_PREF_SIZE);
    }


    @Override
    public void startEdit()
    {
        if (!isEmpty())
        {
            super.startEdit();
            comboBox.setItems(getTableView().getItems().get(getIndex()).getValues());
            comboBox.getSelectionModel().select(getItem());

            comboBox.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue)
                {
                    commitEdit(comboBox.getSelectionModel().getSelectedItem());
                }
            });

            setText(null);
            setGraphic(comboBox);
        }
    }


    @Override
    public void cancelEdit()
    {
        super.cancelEdit();

        setText((String)getItem());
        setGraphic(null);
    }


    @Override
    public void updateItem(String item, boolean empty)
    {
        super.updateItem(item, empty);

        if (empty)
        {
            setText(null);
            setGraphic(null);
        }
        else
        {
            if (isEditing())
            {
                setText(null);
                setGraphic(comboBox);
            }
            else
            {
                String s = comboBox.getSelectionModel().getSelectedItem();
                setText(s);
                getTableView().getItems().get(getIndex()).setSelected(s);
                setGraphic(null);
            }
        }
    }

}

